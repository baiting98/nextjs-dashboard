import '@/app/ui/global.css';
import '@/public/LineIcons.2.0.css';
import '@/public/animate.css';
import '@/public/tiny-slider.css';
import { Metadata, Viewport } from 'next';
 
export const viewport: Viewport = {
  width: 'device-width',
  initialScale: 1,
  maximumScale: 1,
}

export const metadata: Metadata = {
  title: "小V猫 - 免费的自媒体平台多账号管理工具",
  description: "小V猫 - 免费自媒体运营工具，抖音、小红书、视频号、快手等平台多账号管理，文章短视频一键同步分发、各平台数据分析，一站式多账号运营，简单更高效。",
  verification: {
    google: 'PBrQ3WlBFjML7TzQ-FXoY56TFGqjVkPwLrXMMRcq5IA'
  },
  keywords: '一键分发,多账号管理,自媒体工具,自媒体管理,短视频同步,短视频分发',
  icons: {
    shortcut: 'https://alcdn.baoteyun.com/vcat/config/vcat_favicon.png'
  }
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="zh-CN" className="scroll-smooth">
      <body>{children}</body>
    </html>
  );
}
