import Link from 'next/link';
import Image from 'next/image';
import Script from 'next/script';
import { headers } from 'next/headers';

export default function Page() {
  return (<>
    <header id="header-wrap" className="relative">
            
      <div className="navigation fixed top-0 left-0 w-full z-30 duration-300">
          <div className="container">
              <nav className="navbar py-2 navbar-expand-lg flex justify-between items-center relative duration-300">
                  <a className="navbar-brand">
                    <img src="https://alcdn.baoteyun.com/vcat/config/vcat.svg" alt="Logo" />
                  </a>
                  <button className="navbar-toggler focus:outline-none block lg:hidden" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span className="toggler-icon"></span>
                      <span className="toggler-icon"></span>
                      <span className="toggler-icon"></span>
                  </button>

                  <div className="collapse navbar-collapse hidden lg:block duration-300 shadow absolute top-100 left-0 mt-full bg-white z-20 px-5 py-3 w-full lg:static lg:bg-transparent lg:shadow-none" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto justify-center items-center lg:flex">
                      <li className="nav-item">
                        <a className="page-scroll js-page-scroll active" href="#hero-area">下载</a>
                      </li>
                      <li className="nav-item">
                        <a className="page-scroll js-page-scroll" href="#services">功能</a>
                      </li>
                      <li className="nav-item">
                        <a className="page-scroll js-page-scroll" href="#carousel">预览</a>
                      </li>
                      <li className="nav-item">
                        <a className="page-scroll js-page-scroll" href="#contact">反馈</a>
                      </li>
                      <li className="nav-item">
                        <a className="page-scroll" href="https://txc.qq.com/products/638650/faqs-more/" target="_blank">常见问题</a>
                      </li>
                    </ul>
                  </div>
              </nav>
          </div>
      </div>
      
    </header>
    

    
    <section id="hero-area" className="bg-amber-50/70 pt-48 pb-10">
      <div className="container">
        <div className="flex justify-between">
          <div className="w-full text-center">
            <h2 className="text-4xl font-bold leading-snug text-gray-700 mb-10 wow fadeInUp" data-wow-delay="1s">自媒体平台多账号管理、一键分发
              <br className="hidden lg:block" />全功能免费</h2>
            <div className="text-center mb-5 wow fadeInUp" data-wow-delay="1.2s">
              <a href="#" rel="nofollow" className="btn" id="downloadBtn">立即下载</a>
              <p className="pt-5 text-gray-400 text-sm">最新版本：<span id="versionValue"></span></p>
            </div>
            <div className="text-center mb-10 wow fadeInUp" data-wow-delay="1.4s">
              <a href="#" rel="nofollow" id="winLink" className="text-amber-300 underline underline-offset-2">Windows版本</a>
              <a href="#" rel="nofollow" className="text-amber-300 ml-10 underline underline-offset-2" id="macLink">MacOS版本</a>
            </div>
            <div className="text-center wow fadeInUp" data-wow-delay="1.6s">
              <img className="img-fluid mx-auto" src="https://alcdn.baoteyun.com/vcat/www/screenshot_00_s.png" alt="" />
            </div>
          </div>
        </div>
      </div>
    </section>
    

    
    <section id="services" className="py-24">
      <div className="container">
        <div className="text-center">
          <h2 className="mb-12 section-heading wow fadeInDown" data-wow-delay="0.3s">功能</h2>
        </div>
        <div className="flex flex-wrap">
          
          <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
            <div className="m-4 wow fadeInRight" data-wow-delay="0.3s">
              <div className="icon text-5xl">
                <i className="lni lni-users"></i>
              </div>
              <div>
                <h3 className="service-title">一人多号</h3>
                <p className="text-gray-600">可以在一个工具里同时管理多个自媒体平台的账号，同一平台也可以管理多个账号。支持添加视频号、抖音、小红书、快手等，后续持续开发对更多平台的支持。</p>
              </div>
            </div>
          </div>
          
          <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
            <div className="m-4 wow fadeInRight" data-wow-delay="1.2s">
              <div className="icon text-5xl">
                <i className="lni lni-angellist"></i>
              </div>
              <div>
                <h3 className="service-title">一键分发</h3>
                <p className="text-gray-600">同一视频/图文只需设置一次，即可同时发布到所有管理的账号。多个平台，多个账号同时上传发布，省时方便。</p>
              </div>
            </div>
          </div>
          
          <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
            <div className="m-4 wow fadeInRight" data-wow-delay="1.5s">
              <div className="icon text-5xl">
                <i className="lni lni-bookmark-alt"></i>
              </div>
              <div>
                <h3 className="service-title">发布记录</h3>
                <p className="text-gray-600">每次发布都会创建发布记录，便于跟踪和修改重发。</p>
              </div>
            </div>
          </div>
          
          <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
            <div className="m-4 wow fadeInRight" data-wow-delay="0.6s">
              <div className="icon text-5xl">
                <i className="lni lni-bar-chart"></i>
              </div>
              <div>
                <h3 className="service-title">总览数据</h3>
                <p className="text-gray-600">对多个平台的所有账号进行汇总统计，多维度分析粉丝，作品，浏览等数据。</p>
              </div>
            </div>
          </div>
          
          <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
            <div className="m-4 wow fadeInRight" data-wow-delay="0.9s">
              <div className="icon text-5xl">
                <i className="lni lni-briefcase"></i>
              </div>
              <div>
                <h3 className="service-title">本机存储</h3>
                <p className="text-gray-600">添加到工具里的所有账号都存储在本机电脑，不收集和上传用户任何敏感隐私和商业数据。避免数据外泄风险。</p>
              </div>
            </div>
          </div>
          
          <div className="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
            <div className="m-4 wow fadeInRight" data-wow-delay="1.8s">
              <div className="icon text-5xl">
                <i className="lni lni-fresh-juice"></i>
              </div>
              <div>
                <h3 className="service-title">无限免费</h3>
                <p className="text-gray-600">添加管理的账号数量无限制，所有功能无限制，无任何付费和广告。</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    

    
    <section id="carousel" className="bg-amber-50/70 carousel-area py-24">
      <div className="container">
        <div className="text-center">
          <h2 className="mb-12 section-heading">截图预览</h2>
        </div>
        <div className="flex">
          <div className="w-full relative">
            <div className="portfolio-carousel">
              <div>
                <img className="w-full" src="https://alcdn.baoteyun.com/vcat/www/screenshot_01_s.png" alt="" />
              </div>
              <div>
                <img className="w-full" src="https://alcdn.baoteyun.com/vcat/www/screenshot_02_s.png" alt="" />
              </div>
              <div>
                <img className="w-full" src="https://alcdn.baoteyun.com/vcat/www/screenshot_03_s.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    

    
    <section id="contact" className="py-24">    
      <div className="container">
        <div className="text-center">
          <h2 className="mb-12 text-4xl text-gray-700 font-bold tracking-wide wow fadeInDown" data-wow-delay="0.3s">给我们反馈</h2>
        </div>

        <div className="flex flex-wrap justify-center wow fadeInUp" data-wow-delay="0.4s">        
          <div className="flex flex-wrap items-center">
            <div className="contact-icon">
              <i className="lni lni-bug"></i>
            </div>
            <p className="pl-3">
              <a href="https://txc.qq.com/products/638650/" target="_blank" className="underline underline-offset-2">提交产品建议/反馈</a>
            </p>
          </div>
          <div className="flex flex-wrap items-center ml-10">
            <div className="contact-icon">
              <i className="lni lni-envelope"></i>
            </div>
            <p className="pl-3">邮件发送至：xiaobufu01@qq.com</p>
          </div>
        </div>
      </div> 
    </section>
    
    <footer id="footer" className="py-20 pt-60">
      <div className="container">
        <div className="w-full text-center">
          <p className="">© 2024 VCat. All Rights Reserved.</p>
        </div>
      </div>
    </footer> 
    

    
    <a href="#" className="back-to-top w-10 h-10 fixed bottom-0 right-0 mb-5 mr-5 flex items-center justify-center rounded-full bg-amber-400 text-white text-lg z-20 duration-300 hover:bg-amber-500">
      <i className="lni lni-pointer-up"></i>
    </a>

    <Script src="tiny-slider.js" />
    <Script src="main.js" />
  </>);
}
